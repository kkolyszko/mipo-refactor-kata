# SPDX-License-Identifier: BSD-2-Clause

import pytest

from src.interfejsy.rozliczenie import IRozliczenie
from src.koszty.emerytalna import SkladkaEmerytalna
from src.koszty.ubezpieczenie_spoleczne import SkladkaUbezpieczenieSpoleczne


@pytest.fixture(scope="function")
def instance():
    o = SkladkaEmerytalna(100)
    return o


def test_inheritance(instance):
    assert isinstance(instance, SkladkaUbezpieczenieSpoleczne) is True


def test_interface_implementation(instance):
    assert isinstance(instance, IRozliczenie) is True


def test_correct_value(instance):
    result = instance.zwrocWartoscDoRozliczenia()
    assert result == 9.76
