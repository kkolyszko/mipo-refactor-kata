from sys import exit

from src.errory import NieznanaUmowaError
from src.factory import dajUmowe
from src.kontroler import WynagrodzenieKontroler
from src.widoki.cli_stary import CliStaryWidok


def main():
    try:
        wynagrodzenieBrutto = float(input("Podaj kwotę dochodu: "))
    except ValueError:
        exit("Błędna kwota")
    typUmowy = input("Typ umowy: (P)raca, (Z)lecenie, (D)zielo: ")[0]
    try:
        umowa = dajUmowe(typUmowy)
    except NieznanaUmowaError:
        print("Nieznany typ umowy!")

    widok = CliStaryWidok()
    kontroler = WynagrodzenieKontroler(umowa, widok)
    kontroler.oblicz(wynagrodzenieBrutto)
    kontroler.wyswietl()


if __name__ == "__main__":
    exit(main())
