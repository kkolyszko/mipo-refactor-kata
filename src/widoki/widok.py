# SPDX-License-Identifier: BSD-2-Clause

from abc import ABCMeta, abstractmethod
from typing import Callable

from jsonschema import validate

from src.kontrakty.schematy import pobierzSchematJson


class Widok(metaclass=ABCMeta):
    def __init__(self, model=None):
        self._model = model

    @property
    @abstractmethod
    def kontrakt(self) -> str:
        pass

    @property
    def model(self) -> dict:
        return self._model

    @model.setter
    def model(self, model: dict):
        self._model = model

    def zwaliduj(metoda: Callable):  # type: ignore
        # wzorzec decorator
        def zwalidujModel(self):
            schemat = pobierzSchematJson(self.kontrakt)
            validate(instance=self._model, schema=schemat)
            metoda(self)

        return zwalidujModel
