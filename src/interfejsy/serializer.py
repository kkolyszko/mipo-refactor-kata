# SPDX-License-Identifier: BSD-2-Clause

from abc import ABCMeta, abstractmethod


class ISerializer(metaclass=ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return hasattr(subclass, "serializujSie") and callable(subclass.serializujSie)

    @abstractmethod
    def serializujSie(self) -> dict:
        raise NotImplementedError
