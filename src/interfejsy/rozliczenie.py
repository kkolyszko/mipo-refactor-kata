# SPDX-License-Identifier: BSD-2-Clause

from abc import ABCMeta, abstractmethod
from typing import Union


class IRozliczenie(metaclass=ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return hasattr(subclass, "zwrocWartoscDoRozliczenia") and callable(
            subclass.zwrocWartoscDoRozliczenia
        )

    @abstractmethod
    def zwrocWartoscDoRozliczenia(self) -> Union[float, int]:  # albo "float" albo "int"
        raise NotImplementedError
