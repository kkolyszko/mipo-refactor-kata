# SPDX-License-Identifier: BSD-2-Clause

from abc import ABCMeta, abstractmethod


class IKontroler(metaclass=ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (
            hasattr(subclass, "oblicz")
            and callable(subclass.oblicz)
            and hasattr(subclass, "wyswietl")
            and callable(subclass.wyswietl)
        )

    @abstractmethod
    def oblicz(self, wynagrodzenieBrutto: float) -> None:
        raise NotImplementedError

    @abstractmethod
    def wyswietl(self) -> None:
        raise NotImplementedError
