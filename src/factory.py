# SPDX-License-Identifier: BSD-2-Clause

from enum import Enum, unique

from src.errory import NieznanaUmowaError
from src.umowy.dzielo import UmowaDzielo
from src.umowy.praca import UmowaPraca
from src.umowy.umowa import Umowa
from src.umowy.zlecenie import UmowaZlecenie


@unique
class UmowaEnum(Enum):
    P = UmowaPraca
    Z = UmowaZlecenie
    D = UmowaDzielo


def dajUmowe(typUmowy: str) -> Umowa:
    try:
        umowa = UmowaEnum[typUmowy].value
        return umowa()
    except KeyError:
        raise NieznanaUmowaError(typUmowy)
