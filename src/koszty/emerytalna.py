# SPDX-License-Identifier: BSD-2-Clause

from src.interfejsy.rozliczenie import IRozliczenie
from src.koszty.ubezpieczenie_spoleczne import SkladkaUbezpieczenieSpoleczne


class SkladkaEmerytalna(SkladkaUbezpieczenieSpoleczne, IRozliczenie):
    def __init__(self, podstawa: float):
        self._mnoznik = 9.76 / 100
        self._wartosc = podstawa * self._mnoznik

    def zwrocWartoscDoRozliczenia(self):
        return self._wartosc
